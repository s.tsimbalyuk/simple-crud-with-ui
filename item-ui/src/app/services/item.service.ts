import {
  Injectable
} from '@angular/core';
import {
  HttpClient,
  HttpHeaders
} from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable()
export class ItemService {

  constructor(private http: HttpClient) {}

  getItems() {
    const token = localStorage.getItem('access_token');
    return this.http.get('/server/api/v1/items', {
      headers: new HttpHeaders().set('Authorization', 'Bearer ' + token)
      console.log(new HttpHeaders().getAll().toString)
    });
  }

  getItem(id: number) {
    const token = localStorage.getItem('access_token');
    return this.http.get('/server/api/v1/items/' + id, {
      headers: new HttpHeaders().set('Authorization', 'Bearer ' + token)
    });
  }

  createItemRegistration(item) {
    const body = JSON.stringify(item);
    return this.http.post('/server/api/v1/items', body, httpOptions);
  }

}
