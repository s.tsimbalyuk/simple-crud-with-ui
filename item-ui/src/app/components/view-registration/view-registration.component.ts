import { Component, OnInit } from '@angular/core';
import { ItemService } from '../../services/item.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-view-registration',
  templateUrl: './view-registration.component.html',
  styleUrls: ['./view-registration.component.css']
})
export class ViewRegistrationComponent implements OnInit {

  public itemReg;

  constructor(private itemService: ItemService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.getItemReg(this.route.snapshot.params.id);
  }

  getItemReg(id:number) {
    this.itemService.getItem(id).subscribe(
      data => {
        this.itemReg = data;
      },
      err => console.error(err),
      () => console.log('items loaded')
    );
  }

}
