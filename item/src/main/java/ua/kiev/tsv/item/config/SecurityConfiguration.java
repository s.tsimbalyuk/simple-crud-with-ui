package ua.kiev.tsv.item.config;

import com.auth0.spring.security.api.JwtWebSecurityConfigurer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity
@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    @Value(value = "${auth0.consumer}")
    private String consumer;
    @Value(value = "${auth0.issuer}")
    private String issuer;
    @Value(value = "${auth0.secret}")
    private String secret;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        JwtWebSecurityConfigurer
                .forRS256(consumer, issuer)
                .configure(http)
                .authorizeRequests()
                .antMatchers(HttpMethod.POST, "/api/v1/items").permitAll()
                .antMatchers(HttpMethod.GET, "/api/v1/items").hasAuthority("view:registrations")
                .antMatchers(HttpMethod.GET, "/api/v1/items/**").hasAuthority("view:registration")
                .anyRequest().authenticated();
    }
}
