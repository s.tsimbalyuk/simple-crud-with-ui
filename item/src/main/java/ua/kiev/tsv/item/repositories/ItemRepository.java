package ua.kiev.tsv.item.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import ua.kiev.tsv.item.models.Item;

public interface ItemRepository extends JpaRepository<Item, Long> {

}
