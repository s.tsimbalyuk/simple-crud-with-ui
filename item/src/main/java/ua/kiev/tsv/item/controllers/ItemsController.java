package ua.kiev.tsv.item.controllers;

import ua.kiev.tsv.item.models.Item;
import ua.kiev.tsv.item.repositories.ItemRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/items")
public class ItemsController {
    public ItemsController(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    private ItemRepository itemRepository;


    @GetMapping
    public List<Item> list() {
        return itemRepository.findAll();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public void create(@RequestBody Item item) {
        System.out.println(item.getName());
        itemRepository.save(item);
    }

    @GetMapping("/{id}")
    public Item get(@PathVariable("id") long id) {
        return itemRepository.getOne(id);
    }

}
